<?php
$menu['produccion'] = [
    [
		'nombre'	=>	'Produccion',
		'direccion'	=>	'#produccion',
		'icono'		=>	'fa fa-folder',
        'menu'      =>  [

            [
                'nombre'    => 'Productores',
                'direccion' =>  'produccion/productor',
                'icono'     =>  'fa fa-quote-right'
            ],
            [
                'nombre'    =>  'Rubros',
                'direccion' =>  'produccion/rubros',
                'icono'     =>  'fa fa-share-alt'
            ],
            
			[
                'nombre'    =>  'Relación de cosecha',
                'direccion' =>  'produccion',
                'icono'     =>  'fa fa-pencil-square-o'
            ]

        ]
	]
];
 ?>
