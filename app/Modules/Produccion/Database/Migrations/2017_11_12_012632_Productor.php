<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Productor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productor', function (Blueprint $table) {
			$table->increments('id');
			$table->string('cedula', 250);
			$table->string('nombre', 250);
			$table->string('telefono', 250);
			$table->integer('municipios_id')->unsigned();
			$table->integer('parroquias_id')->unsigned();
			$table->string('sector', 250);
			$table->string('unidad_prod', 250);

            $table->foreign('municipios_id')
				->references('id')->on('municipios')
				->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parroquias_id')
				->references('id')->on('parroquias')
				->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productor');
    }
}
