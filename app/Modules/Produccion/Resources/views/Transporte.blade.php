@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Transporte']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Transporte.',
        'columnas' => [
            'Arrime Cvg' => '16.666666666667',
		
		'Numero Guia' => '16.666666666667',
		'Fundo Pilones Mercados' => '16.666666666667'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Transporte->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection