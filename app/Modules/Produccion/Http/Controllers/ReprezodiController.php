<?php

namespace App\Modules\Produccion\Http\Controllers;

//Controlador Padre
use App\Modules\Produccion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Produccion\Http\Requests\ReprezodiRequest;

//Modelos
use App\Modules\Produccion\Models\Reprezodi;

class ReprezodiController extends Controller
{
    protected $titulo = 'Reprezodi';

    public $js = [
        'Reprezodi'
    ];
    
    public $css = [
        'Reprezodi'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('produccion::Reprezodi', [
            'Reprezodi' => new Reprezodi()
        ]);
    }

    public function nuevo()
    {
        $Reprezodi = new Reprezodi();
        return $this->view('produccion::Reprezodi', [
            'layouts' => 'base::layouts.popup',
            'Reprezodi' => $Reprezodi
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Reprezodi = Reprezodi::find($id);
        return $this->view('produccion::Reprezodi', [
            'layouts' => 'base::layouts.popup',
            'Reprezodi' => $Reprezodi
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Reprezodi = Reprezodi::withTrashed()->find($id);
        } else {
            $Reprezodi = Reprezodi::find($id);
        }

        if ($Reprezodi) {
            return array_merge($Reprezodi->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ReprezodiRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Reprezodi = $id == 0 ? new Reprezodi() : Reprezodi::find($id);

            $Reprezodi->fill($request->all());
            $Reprezodi->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Reprezodi->id,
            'texto' => $Reprezodi->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Reprezodi::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Reprezodi::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Reprezodi::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Reprezodi::select([
            'id', 'nombre', 'dni', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}