<?php

namespace App\Modules\Produccion\Http\Requests;

use App\Http\Requests\Request;

class ReprezodiRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:250'], 
		'cedula' => ['required', 'min:3', 'max:250']
	];
}