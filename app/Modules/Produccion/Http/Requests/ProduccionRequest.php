<?php

namespace App\Modules\Produccion\Http\Requests;

use App\Http\Requests\Request;

class ProduccionRequest extends Request {
    protected $reglasArr = [
		'productor_id' => ['required', 'integer'], 
		'rubros_id' => ['required', 'integer'], 
		'ente_crediticio' => ['required', 'min:3', 'max:250'], 
		'het_def' => ['required', 'min:3', 'max:250'], 
		'estimacion' => ['required', 'min:3', 'max:250'], 
		'produccion_kg' => ['required', 'min:3', 'max:250'], 
		 
		'tec_resp' => ['required', 'min:3', 'max:250'], 
		'total_produccion' => ['required', 'min:3', 'max:250'],
		'arrime_agroindustria' => ['required', 'min:3', 'max:250'],
		'arrime_silo' => ['required', 'min:3', 'max:250'],
		'numero_guia' => ['required', 'min:3', 'max:250'],
		'estados_id'  => ['required', 'integer'],
		'fundo_pilones_mercados' => ['required', 'min:3', 'max:250']
	];
}