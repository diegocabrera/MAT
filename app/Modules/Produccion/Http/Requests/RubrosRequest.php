<?php

namespace App\Modules\Produccion\Http\Requests;

use App\Http\Requests\Request;

class RubrosRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:250'], 
		'tipo' => ['required', 'min:3', 'max:250'],
		'variedad' => ['required', 'min:3', 'max:250'],
	];
}