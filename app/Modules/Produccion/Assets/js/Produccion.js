var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"fecha_siembra","name":"fecha_siembra"},
				  {"data":"fecha_cosecha","name":"fecha_cosecha"},
				  {"data":"fecha_arrime","name":"fecha_arrime"},
				  {"data":"productor_id","name":"productor_id"},
				  {"data":"rubros_id","name":"rubros_id"},
				  {"data":"ente_crediticio","name":"ente_crediticio"},
				  {"data":"het_def","name":"het_def"},
				  {"data":"arrime_agroindustria","name":"arrime_agroindustria"},
				  {"data":"arrime_silo","name":"arrime_silo"},
				  {"data":"estimacion","name":"estimacion"},
				  {"data":"produccion_kg","name":"produccion_kg"}
				  ,{"data":"n_estimacion","name":"n_estimacion"},
				  {"data":"knsa","name":"knsa"},
				  {"data":"kna","name":"kna"},
				  {"data":"condicion","name":"condicion"},
				  {"data":"fundo_pilones_mercados","name":"fundo_pilones_mercados"},
				  //{"data":"tec_resp","name":"tec_resp"},
				  {"data":"numero_guia","name":"numero_guia"},
				  {"data":"N_deplanilla","name":"N_deplanilla"},
				  {"data":"total_produccion","name":"total_produccion"}]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#fecha_arrime', $form).datetimepicker();
	$('#fecha_siembra', $form).datetimepicker();
	$('#fecha_cosecha', $form).datetimepicker();
});
