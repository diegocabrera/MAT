var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();

		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"cedula","name":"cedula"},
			{"data":"nombre","name":"nombre"},
			{"data":"telefono","name":"telefono"},
			{"data":"municipios_id","name":"municipios_id"},
			{"data":"parroquias_id","name":"parroquias_id"},
			{"data":"sector","name":"sector"},
			{"data":"unidad_prod","name":"unidad_prod"}
		]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
	$('#municipios_id').change(function() {
        aplicacion.selectCascada($(this).val(), 'parroquias_id', 'parroquia');

    });
});
