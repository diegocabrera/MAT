<?php

namespace App\Modules\Produccion\Models;

use App\Modules\base\Models\Modelo;



class Reprezodi extends Modelo
{
    protected $table = 'reprezodi';
    protected $fillable = ["nombre","cedula"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Representante'
    ],
    'cedula' => [
        'type' => 'text',
        'label' => 'Documento de Identidad',
        'placeholder' => 'Documento de Identidad del Representante'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }


}
