<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\base\Models\Modelo;

use App\Modules\Produccion\Models\Productor;
use App\Modules\Produccion\Models\Rubros;
use App\Modules\Produccion\Models\Reprezodi;
use App\Modules\Produccion\Models\Transporte;
use App\Modules\Base\Models\estados;

class Produccion extends Modelo
{
    protected $table = 'produccion';
    protected $fillable = [
    "fecha_cosecha",
    "fecha_arrime",
    "fecha_siembra",
    "productor_id",
    "rubros_id",
    "ente_crediticio",
    "arrime_agroindustria",
    "het_def",
    "estimacion",
    "produccion_kg",
    "tec_resp",
    "total_produccion",
    "arrime_silo",
    "numero_guia",
    "guia_fuera",
    "fundo_pilones_mercados",
    "knsa",
    "kna",
    "rendimiento",
    "arrime_silo",
    "silo",
    "condicion",
    "N_deplanilla",
    "numero_guia", 
    "estados_id",


];
    protected $campos = [
    
    'productor_id' => [
        'type' => 'select',
        'label' => 'Productor',
        'placeholder' => '- Seleccione un Productor',
        'url' => 'productor'
    ],
    'rubros_id' => [
        'type' => 'select',
        'label' => 'Rubros',
        'placeholder' => '- Seleccione un Rubros',
        'url' => 'rubros'
    ],
    'ente_crediticio' => [
        'type' => 'text',
        'label' => 'Ente Crediticio',
        'placeholder' => 'Ente Crediticio del Produccion'
    ],
    'arrime_agroindustria' => [
        'type' => 'text',
        'label' => 'Arrime Agroindustria',
        'placeholder' => 'Arrime Agroindustria del Transporte'
    ],
    'fecha_siembra' => [
        'type' => 'text',
        'label' => 'Fecha Siembra',
        'placeholder' => 'Fecha Siembra'
    ],
    'fecha_cosecha' => [
        'type' => 'text',
        'label' => 'Fecha cosecha',
        'placeholder' => 'Fecha de cosecha'
    ],
    'fecha_arrime' => [
        'type' => 'text',
        'label' => 'Fecha arrime',
        'placeholder' => 'Fecha de arrime'
    ],
    'het_def' => [
        'type' => 'text',
        'label' => 'Ha Def',
        'placeholder' => 'Hectareas Definidas'
    ],
    'estimacion' => [
        'type' => 'text',
        'label' => 'Estimacion',
        'placeholder' => 'Estimacion de la Produccion'
    ],
    'produccion_kg' => [
        'type' => 'text',
        'label' => 'Produccion Kg y ton',
        'placeholder' => 'Produccion Kg o toneladas de la Produccion'
    ],
    'rendimiento' => [
        'type' => 'text',
        'label' => 'rendimiento',
        'placeholder' => 'rendimiento'
    ],
    'arrime_silo' => [
        'type' => 'text',
        'label' => 'arrime ',
        'placeholder' => 'arrime'
    ],
    'silo' => [
        'type' => 'text',
        'label' => 'silo ',
        'placeholder' => 'silo'
    ],
    'knsa' => [
        'type' => 'text',
        'label' => 'knsa',
        'placeholder' => 'knsa'
    ],
    'kna' => [
        'type' => 'text',
        'label' => 'kna',
        'placeholder' => 'kna'
    ],
    'condicion' => [
        'type' => 'text',
        'label' => 'condicion del grano',
        'placeholder' => 'condicion del grano'
    ],
    'tec_resp' => [
        'type' => 'text',
        'label' => 'Tec Resp',
        'placeholder' => 'Tec Resp del Produccion'
    ],
    'N_deplanilla' => [
        'type' => 'text',
        'label' => 'planilla de recepcion',
        'placeholder' => 'N° de planilla de recepcion'
    ],
    'numero_guia' => [
        'type' => 'text',
        'label' => 'Numero Guia',
        'placeholder' => 'Numero Guia del Transporte'
    ],
    'estados_id' => [
       'type' => 'select',
        'label' => 'estado',
        'placeholder' => '- Seleccione un estado',
        'url' => 'estados'
    ],   
     
    
    'total_produccion' => [
        'type' => 'text',
        'label' => 'Total Produccion',
        'placeholder' => 'Total Produccion del Produccion'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['productor_id']['options'] = Productor::pluck('nombre', 'id');
		$this->campos['rubros_id']['options'] = Rubros::pluck('nombre', 'id');
	    $this->campos['estados_id']['options'] = estados::pluck('nombre', 'id');
    }
    public function setPublishedAtAttribute($value){
    $this->attributes['fecha_siembra'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    $this->attributes['fecha_cosecha'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    $this->attributes['fecha_arrime'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    }
    public function productor()
    {
        return $this->belongsTo('App\Modules\Produccion\Models\Productor');
    }

    public function rubros()
    {
        return $this->belongsTo('App\Modules\Produccion\Models\Rubros');
    }

    public function reprezodi()
    {
        return $this->belongsTo('App\Modules\Produccion\Models\Reprezodi');
    }

    public function transporte()
    {
        return $this->belongsTo('App\Modules\Produccion\Models\Transporte');
    }


}
