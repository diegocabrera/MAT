<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\base\Models\Modelo;


class Rubros extends Modelo
{
    protected $table = 'rubros';
    protected $fillable = ["nombre","tipo","variedad"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Rubros'
    ],
    'tipo' => [
        'type' => 'text',
        'label' => 'Tipo',
        'placeholder' => 'Tipo del Rubros'
    ],
    'variedad' => [
        'type' => 'text',
        'label' => 'variedad',
        'placeholder' => 'variedad de rubros'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    public function produccion()
    {
        return $this->hasMany('App\ModulesProduccion\Models\Produccion');
    }


}
